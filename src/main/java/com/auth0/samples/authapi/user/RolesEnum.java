package com.auth0.samples.authapi.user;

public enum RolesEnum {
	ROLE_ADMIN,
	ROLE_USER
}
