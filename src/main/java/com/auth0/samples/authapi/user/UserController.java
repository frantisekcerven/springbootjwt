package com.auth0.samples.authapi.user;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

	private ApplicationUserRepository applicationUserRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserController(ApplicationUserRepository applicationUserRepository,
						  BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.applicationUserRepository = applicationUserRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@PostMapping("/sign-up")
	public void signUp(@RequestBody ApplicationUser user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		applicationUserRepository.save(user);
	}

	@GetMapping("/sign-up-test-data")
	public void signUpGet() {
		ApplicationUser user = new ApplicationUser();
		user.setUsername("fero");
		user.setPassword(bCryptPasswordEncoder.encode("123"));
		user.setRoles(RolesEnum.ROLE_ADMIN.name());
		applicationUserRepository.save(user);

		ApplicationUser user2 = new ApplicationUser();
		user2.setUsername("tono");
		user2.setPassword(bCryptPasswordEncoder.encode("123"));
		user2.setRoles(RolesEnum.ROLE_USER.name());
		applicationUserRepository.save(user2);
	}

	@GetMapping()
	public List<ApplicationUser> getAllUsers() {
		return applicationUserRepository.findAll();
	}
}
